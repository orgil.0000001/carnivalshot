using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Table : Mb {
    public Vector3 a, b;
    public float t = 0, spd = 1;
    bool isInit = false;
    public void Init(Vector3 a, Vector3 b, float t, float spd) {
        this.a = a;
        this.b = b;
        this.t = t;
        this.spd = spd;
        isInit = true;
    }
    void Update() {
        if (isInit) {
            t += Dt * 0.2f;
            Tp = V3.Lerp(a, b, M.PingPong01(t));
            if (Tcc == 1) {
                isInit = false;
                go.An().enabled = true;
                Dst(go, 0.3f);
            }
        }
    }
    IEnumerator DstCor(float tm) {
        isInit = false;
        yield return Wf.S(tm);
        Dst(go);
    }
}
