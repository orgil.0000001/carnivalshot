using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using Polygon;

public class PolygonChr : Mb {
    [HideInInspector]
    public float spd = 1, minAng = 0, maxAng = 0;
    Arrow arrowPf;
    Transform shotPntTf;
    [HideInInspector]
    public GameObject chrGo, wepGo, fireGo;
    Animator an;
    Arrow arrow;
    bool isBow = false, isDraw = false, isAim = false, isUp = false, isRifle = false;
    float drawSpd = 1;
    float chrScl => chrGo.PolyScl();
    void Awake() {
    }
    void Start() {
    }
    private void Update() {
        if (IsPlaying) {
            if (isBow) {
                if (IsMbD && !isDraw && !isUp)
                    Play(Anim.Longbow_Draw_Arrow);
                if (IsMbU && isDraw)
                    isUp = true;
                if (isUp && isDraw && isAim)
                    Play(Anim.Longbow_Aim_Recoil);
            }
        }
    }
    public void Chr(PolyChr chr) {
        chrGo = Polygon.Poly.CrtChr(tf, A.Load<RuntimeAnimatorController>("Animations/Human"), chr);
        Cm.I.UpdOff(chrGo.PolyBodyPart(PolyBodyPart.Toes_L), chrGo.PolyBodyPart(PolyBodyPart.Toes_R));
        an = chrGo.An();
        if (chr == PolyChr.PolygonFantasyKingdom_SM_Chr_Fairy_01)
            StaCor(FairyWingsCor());
    }
    public void Atcs(params PolyAtc[] atcs) { Polygon.Poly.CrtAtcs(chrGo, atcs); }
    public void Wep(PolyWep wep) { wepGo = Polygon.Poly.CrtWep(chrGo, wep); }
    public void Bow(int bowType = 1, bool isPut = false) { wepGo = Polygon.Poly.CrtBow(chrGo, ref arrowPf, ref shotPntTf, bowType, isPut); }
    public void Torch(bool isPut = false) { wepGo = Polygon.Poly.CrtTorch(chrGo, isPut); }
    public void TorchFire(bool isPrewarm = true) { fireGo = Polygon.Poly.AddTorchFire(wepGo, isPrewarm); }
    public void Melee(PolyWep wep, bool isPut = false, bool isUnderarm = true) { wepGo = Polygon.Poly.CrtMelee(chrGo, wep, isPut, isUnderarm); }
    public void GreatSword(PolyWep wep, bool isPut = false) { wepGo = Polygon.Poly.CrtGreatSword(chrGo, wep, isPut); }
    public void SwordAndShield(PolyWep wep, PolyWep shield = PolyWep.Null, bool isPut = false) { wepGo = Polygon.Poly.CrtSwordAndShield(chrGo, wep, shield, isPut); }
    public void Play(Anim anim, PolyFX fx = PolyFX.Null, PolyFX fx2 = PolyFX.Null) { Play(anim, fx, fx2, PolyWep.Null, 0, 0, 0, false, false); }
    public void Play(Anim anim, PolyWep wep, PolyFX expFx = PolyFX.Null, float expDstTime = 10, float ang = 30, float pwr = 10) { Play(anim, expFx, PolyFX.Null, wep, expDstTime, ang, pwr, false, false); }
    public void PlayChr(Anim anim) { Play(anim, PolyFX.Null, PolyFX.Null, PolyWep.Null, 0, 0, 0, true, false); }
    public void PlayWep(Anim anim) { Play(anim, PolyFX.Null, PolyFX.Null, PolyWep.Null, 0, 0, 0, false, true); }
    public void PlayChrWep(Anim anim) { Play(anim, PolyFX.Null, PolyFX.Null, PolyWep.Null, 0, 0, 0, true, true); }
    public void Play(Anim anim, PolyFX fx, PolyFX fx2, PolyWep wep, float expDstTime, float ang, float pwr, bool isChr, bool isWep) {
        if (Polygon.Poly.AnimDic[anim].type == Anim.MagicAttack) {
            StaCor(MagicCor(anim, fx, fx2));
        } else if (Polygon.Poly.AnimDic[anim].type == Anim.DisarmEquip) {
            StaCor(WepDisarmEquipCor(anim));
        } else if (Polygon.Poly.AnimDic[anim].type == Anim.ThrowGrenade) {
            StaCor(ThrowGrenadeCor(anim, wep, fx, expDstTime, ang, pwr));
        } else if (Polygon.Poly.AnimDic[anim].type == Anim.FrankMage) {
            StaCor(FrankMageCor(anim));
        } else if (anim == Anim.Frank_RPG_Mage_Equip) {
            StaCor(FrankMageEquipCor());
        } else if (anim == Anim.Frank_RPG_Mage_Unequip) {
            StaCor(FrankMageDisarmCor());
        } else if (anim == Anim.Longbow_Draw_Arrow) {
            StaCor(ArrowDrawCor());
        } else if (anim == Anim.Longbow_Aim_Recoil) {
            StaCor(ArrowShotCor());
        } else {
            an.AnimChrWep(anim, wepGo, isChr, isWep);
        }
    }
    IEnumerator FrankMageEquipCor() {
        an.AnimChr(Anim.Frank_RPG_Mage_Equip);
        yield return Wf.F(20);
        wepGo.Tlp(V3.V(0.6f, 0.4f, 0.3f) * chrScl);
        Vector3 a, b;
        float t0, t;
        for (a = V3.Y(3), b = V3.I, t0 = Time.time, t = 0.15f; Time.time - t0 < t;) {
            wepGo.Tls(V3.Lerp(a, b, (Time.time - t0) / t));
            yield return null;
        }
        wepGo.Tls(b);
        for (a = wepGo.Tlp(), b = Polygon.Poly.PosRH * chrScl, t0 = Time.time, t = 0.07f; Time.time - t0 < t;) {
            wepGo.Tlp(V3.Lerp(a, b, (Time.time - t0) / t));
            yield return null;
        }
        wepGo.Tlp(b);
        yield return Wf.S(0.7f);
    }
    IEnumerator FrankMageDisarmCor() {
        Dst(wepGo.Child(0).Col());
        an.AnimChr(Anim.Frank_RPG_Mage_Unequip);
        yield return Wf.F(16);
        Vector3 a, b;
        float t0, t;
        for (a = V3.I, b = V3.Y(3), t0 = Time.time, t = 0.1f; Time.time - t0 < t;) {
            wepGo.Tls(V3.Lerp(a, b, (Time.time - t0) / t));
            yield return null;
        }
        wepGo.Tls(b);
        yield return Wf.S(0.6f);
    }
    IEnumerator FrankMageCor(Anim anim) {
        anim = Anim.Frank_RPG_Mage_Combo01_2;
        AnimData ad = Polygon.Poly.AnimDic[anim];
        an.AnimChr(anim);
        UpdTf(anim, Anim.Frank_RPG_Mage_Idle);
        yield return Wf.S(0.5f);
        Vector3 p = V3.V(-0.2f, 1.15f, 1);
        GameObject fxGo = Ins(Polygon.Poly.GetFxPf(PolyFX.PolygonParticles_FX_PowerBeam_01), go.TfPnt(p), Tr, tf);
        yield return Wf.S(0.5f);
        fxGo.PsStop();
        Dst(fxGo, 1);
        yield return null;
    }
    IEnumerator MagicCor(Anim anim, PolyFX fx = PolyFX.Null, PolyFX fx2 = PolyFX.Null) {
        AnimData ad = Polygon.Poly.AnimDic[anim];
        fx = fx != PolyFX.Null ? fx : ad.fx;
        bool isShard = fx.tS().Contains("Shard"), isExp = fx.tS().Contains("Explosion");
        fx2 = isShard ? PolyFX.Null : fx2 != PolyFX.Null ? fx2 : ad.fx2;
        GameObject fx2Go = null;
        if (fx2 != PolyFX.Null) {
            fx2Go = Ins(Polygon.Poly.GetFxPf(fx2), ad.p2, Tr, ad.part == PolyBodyPart.Null ? tf : chrGo.PolyBodyPart(ad.part));
            if (ad.part != PolyBodyPart.Null)
                fx2Go.Tls(V3.V(chrScl));
        }
        an.AnimChr(anim);
        yield return Wf.S(ad.t);
        if (fx2Go)
            fx2Go.PsStop();
        GameObject fxGo = Ins(Polygon.Poly.GetFxPf(fx), go.TfPnt(isShard ? isExp ? Polygon.Poly.AnimShardExpPos : Polygon.Poly.AnimShardShootPos : ad.p), Tr, tf);
        yield return Wf.S(isShard ? 3.5f : ad.t2);
        fxGo.PsStop();
        if (fx2Go)
            Dst(fx2Go, 1);
        Dst(fxGo, 1);
    }
    IEnumerator WepDisarmEquipCor(Anim anim) {
        AnimData ad = Polygon.Poly.AnimDic[anim];
        an.AnimChr(anim);
        chrGo.Tlp(ad.p);
        yield return Wf.S(ad.t);
        wepGo.Par(chrGo.PolyBodyPart(ad.wpd.part));
        wepGo.PolyTf(ad.wpd.p, ad.wpd.r, chrScl);
        if (anim == Anim.Torch_Unarmed_Equip_2) {
            yield return Wf.S(0.6f);
            fireGo.Show();
        }
        if (ad.a2 != Anim.Null) {
            chrGo.Tlp(ad.p2);
            an.AnimChr(ad.a2);
        }
        yield return Wf.S(ad.t2);
    }
    IEnumerator ThrowGrenadeCor(Anim anim, PolyWep wep, PolyFX expFx, float expDstTime, float ang, float pwr) {
        if (anim.tS().Contains("Throw_Grenade")) {
            an.AnimChrWep(anim, wepGo);
            yield return Wf.S(0.03f);
            wepGo.Par(chrGo.PolyHandL());
            yield return Wf.S(Polygon.Poly.AnimDic[anim].t - 0.05f);
            GameObject parGo = new GameObject("Grenade");
            parGo.Par(chrGo.PolyHandR());
            parGo.PolyTf(V3.V(0.1f, 0.03f, -0.05f), V3.V(-90, 0, -10), chrScl);
            GameObject grenadeGo = Ins(Polygon.Poly.GetWep(wep), parGo.transform);
            PolyWepData pwd = Polygon.Poly.PolyWepDic[wep];
            grenadeGo.PolyTf(pwd.tf.p, pwd.tf.r);
            yield return Wf.S(Polygon.Poly.AnimDic[anim].t2);
            grenadeGo.Par(null);
            Dst(parGo);
            grenadeGo.TpD(F * 0.3f);
            if (expFx != PolyFX.Null) {
                grenadeGo.Ac<Grenade>().Shot(expFx, expDstTime);
            }
            Rigidbody rb = grenadeGo.Ac<Rigidbody>();
            rb.V(go.TfPnt(0, M.Sin(ang), M.Cos(ang)).normalized * pwr);
            BoxCollider bc = grenadeGo.Ac<BoxCollider>();
            yield return Wf.S(1.5f);
            wepGo.Par(chrGo.PolyHandR());
        }
    }
    IEnumerator ArrowDrawCor() {
        isDraw = true;
        an.AnimChr(Anim.Longbow_Draw_Arrow);
        yield return Wf.F(18 / drawSpd);
        arrow = Ins(arrowPf, chrGo.PolyBodyPart(Polygon.Poly.ArrowData.part));
        arrow.go.PolyTf(Polygon.Poly.ArrowData.p, Polygon.Poly.ArrowData.r, chrScl);
        int n = M.RoundI(16 / drawSpd);
        WaitForSeconds wfs = Wf.F(16 / drawSpd / n);
        for (int i = 0; i <= n; i++) {
            arrow.Tle = Ang.Lerp(Polygon.Poly.ArrowData.r, Polygon.Poly.ArrowData.r2, i.F() / n);
            yield return wfs;
        }
        arrow.shotPntTf = shotPntTf;
        yield return Wf.F(14 / drawSpd);
        isAim = true;
    }
    IEnumerator ArrowShotCor() {
        isDraw = false;
        an.AnimChr(Anim.Longbow_Aim_Recoil);
        yield return Wf.F(8 / drawSpd);
        arrow.shotPntTf = null;
        shotPntTf.Tlp(0, 0, -0.12f);
        arrow.Tr = Tr;
        arrow.Shot(10);
        yield return Wf.F(14 / drawSpd);
        isUp = false;
        isAim = false;
    }
    IEnumerator FairyWingsCor() {
        GameObject wingsGo = chrGo.PolySpine3().ChildGo(3, 0), wingLGo = wingsGo.ChildGo(0), wingRGo = wingsGo.ChildGo(1);
        while (true) {
            for (int i = 0; i < 2; i++) {
                float t = Time.time;
                while (Time.time - t < spd) {
                    float a = i == 0 ? maxAng : minAng, b = i == 0 ? minAng : maxAng, s = (Time.time - t) / spd;
                    wingLGo.Tle(-180, Ang.Lerp(a, b, s), 0);
                    wingRGo.Tle(0, Ang.Lerp(-a, -b, s), 0);
                    yield return null;
                }
                wingLGo.Tle(-180, i == 0 ? minAng : maxAng, 0);
                wingRGo.Tle(0, i == 0 ? -minAng : -maxAng, 0);
            }
        }
    }
    void UpdTf(Anim anim, Anim nxtAnim) {
        StaCor(AnimEndCor(anim.tS(), UpdTfCor(anim, nxtAnim)));
    }
    IEnumerator UpdTfCor(Anim anim, Anim nxtAnim) {
        AnimData ad = Polygon.Poly.AnimDic[anim], nxtAd = Polygon.Poly.AnimDic[nxtAnim];
        chrGo.Tlp(V3.O);
        an.AnimChr(nxtAnim);
        Tlp = chrGo.TfPnt(-ad.p);
        yield return null;
    }
    public IEnumerator AnimEndCor(string name, IEnumerator endCor) {
        while (!an.GetCurrentAnimatorStateInfo(0).IsName(name))
            yield return null;
        while (an.GetCurrentAnimatorStateInfo(0).normalizedTime % 1 < 0.99f)
            yield return null;
        StaCor(endCor);
    }
}