using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Ball : Mb {
    public bool isLast = false;
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Power)) {
            Player.I.Power();
            Dst(other.gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.Tag(Tag.Block) || collision.Tag(Tag.Wall)) {
            StaCor(DstCor(2));
        }
    }
    IEnumerator DstCor(float tm) {
        for (float t = 0; t < tm; t += Dt) {
            Tls = V3.V(M.Smt(1, 0, t / tm));
            yield return null;
        }
        Dst(rb);
        go.Mr().enabled = false;
        if (isLast)
            Gc.I.GameOver();
        Dst(go, 3);
    }
}
