﻿using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

[System.Serializable]
public class CannonData {
    public GameObject fireGo, fireEmberGo, elecGo;
    public Transform shotPntTf;
}

public class Player : Character {
    public static Player I {
        get {
            if (_.Null())
                _ = GameObject.FindObjectOfType<Player>();
            return _;
        }
    }
    static Player _;
    PolygonChr chr;
    bool isSta = false, isAim = true;
    public float powerBarSpd = 1;
    public Vector2 g = V2.V(-15, -35), ballMass = V2.V(0.1f, 2);
    public Color perfectC, perfectShadowC, goodC, goodShadowC;
    public CannonData cannon, cannonL, cannonR;
    public Ball ballPf;
    public PowerBar powerBar;
    public Vector3 rot;
    public Vector2 mpSpd = V2.V(0.02f);
    public int shotCnt = 0;
    float rotLimX = 30;
    int perfectCnt = 0;
    bool isPower = false;
    RaycastHit hit;
    private void Awake() {
        _ = this;
        chr = go.Gc<PolygonChr>();
    }
    public void Reset() {
    }
    void Start() {
        rot = V3.X(Ang.Rep180(Te.x));
    }
    void Update() {
        if (IsPlaying && shotCnt < Ls.I.ballCnt) {
            if (isSta) {
                if (isAim) {
                    if (IsMbD)
                        mp = Mp;
                    if (IsMb) {
                        rot = V3.Xy(M.C(rot.x + (mp.y - Mp.y) * mpSpd.x, -55, 8), M.C(rot.y + (Mp.x - mp.x) * mpSpd.y, -rotLimX, rotLimX));
                        Te = rot;
                        mp = Mp;
                    }
                    if (IsMbU) {
                        powerBar.power = 0;
                        powerBar.Show();
                        isAim = false;
                    }
                } else {
                    if (IsMbD) {
                        Shot();
                        StaCor(PowerBarHideCor());
                    }
                    if (IsMbU)
                        isAim = true;
                }
            } else {
                if (IsMbU)
                    isSta = true;
            }
        }
    }
    void CrtTxt(string word, bool isPerfect) {
        GameObject txtGo = Ins(A.LoadGo("Main/TextUi"), Cc.I.hud.transform);
        txtGo.Child(0).Txt().color = isPerfect ? perfectC : goodC;
        txtGo.Child(0).Shadow().effectColor = isPerfect ? perfectShadowC : goodShadowC;
        Gc.I.AnimTxtDst(txtGo, new List<Fo>() { Fo.F(0).S(0), Fo.F(10).S(1.2f), Fo.F(13).S(1), Fo.F(23).S(1), Fo.F(30).S(0) }, true, word, isPerfect);
    }
    public void Power() {
        if (!isPower) {
            isPower = true;
            go.An().enabled = true;
            CrtTxt("POWER UP", true);
        }
    }
    void Shot() {
        List<CannonData> cannons = isPower ? List(cannon, cannonL, cannonR) : List(cannon);
        for (int i = 0; i < cannons.Count; i++) {
            Ball ball = Ins(ballPf, cannons[i].shotPntTf.position, Tr, Gc.I.tf);
            float p = powerBar.Pwr;
            if (p > M._8_9) {
                perfectCnt++;
                if (perfectCnt >= 2) {
                    cannons[i].fireGo.PsEmRot(perfectCnt == 2 ? 20 : perfectCnt == 3 ? 40 : 60);
                    cannons[i].fireEmberGo.PsEmRot(perfectCnt == 2 ? 10 : perfectCnt == 2 ? 20 : 30);
                    if (perfectCnt > 3)
                        cannons[i].elecGo.Show();
                }
                if (i == 0)
                    CrtTxt(Rnd.List(A.PerfectWords) + (perfectCnt == 1 ? "" : "x" + perfectCnt), true);
                ball.ChildShow(1);
            } else {
                perfectCnt = 0;
                cannons[i].fireGo.PsEmRot(0);
                cannons[i].fireEmberGo.PsEmRot(0);
                cannons[i].elecGo.Hide();
                if (p > M._2_3) {
                    if (i == 0)
                        CrtTxt("", false);
                    ball.ChildShow(0);
                }
            }
            float p2 = p + perfectCnt * 0.05f;
            A.G = V3.Y(M.Remap(p2, 0, 1, g.x, g.y));
            ball.rb.mass = M.Remap(p2, 0, 1, ballMass.x, ballMass.y);
            ball.Tls(V3.V(1 + (perfectCnt <= 1 ? 0 : perfectCnt == 2 ? 0.5f : 1)));
            Ray ray = Camera.main.ScreenPointToRay(V3.Xy(Screen.width / 2, Screen.height / 2));
            if (Physics.Raycast(ray, out hit)) {
                ball.rb.V(Hpm.Ang_V0(ball.Tp, hit.point, M.C(V3.Ang(hit.point - ball.Tp, (hit.point - ball.Tp).Y(0)) + 5, 20, 90)));
            } else {
                ball.rb.V(F * 30);
            }
            if (i == 0) {
                shotCnt++;
                if (Ls.I.ballCnt == shotCnt)
                    ball.isLast = true;
            }
        }
        Cc.I.Ball();
    }
    IEnumerator PowerBarHideCor() {
        powerBar.An().Play("Hide");
        yield return Wf.F(10);
        powerBar.Hide();
    }
}