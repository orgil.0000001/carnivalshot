﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using Polygon;

public class Test : Mb {
    public string s;
    [ContextMenu("Print Ps Data")]
    public void PrintPsData() {
        // PrintPs("PolygonParticles");
        PrintPs("ErbGameArt");
    }
    void PrintPs(string packName) {
        List<string> list = new List<string>();
        for (int i = 0; i < Tcc; i++) {
            GameObject childGo = go.ChildGo(i);
            string ss = "";
            Do(childGo, childGo, ref ss, true);
            if (!ss.IsS("new"))
                ss = "new TreeData<string>(" + ss + ")";
            list.Add(Spc(12) + "{ PolyFX." + packName + "_" + childGo.name + "," + Spc(50 - childGo.name.Length) + "Pfd.C.Set(" + ss + ") },\n");
        }
        list.Sort();
        s = list.S("");
        print(s);
    }
    string Spc(int n) {
        string s = "";
        for (int i = 0; i < n; i++)
            s += " ";
        return s;
    }
    void Do(GameObject a, GameObject b, ref string s, bool isTree) {
        bool isPs = a.Ps();
        int tcc = a.Tcc(), idx = a.SibIdx();
        if (a != b && ((a.Par().Tcc() > 0 && idx == 0) || idx > 0))
            s += ", ";
        if (isPs) {
            if (isTree)
                s += "new TreeData<string>(";
            s += "\"" + a.PsMain().startColor.color.Hex() + "\"";
            bool isTree2 = false;
            for (int i = 0; i < tcc && !isTree2; i++)
                isTree2 = a.ChildGo(i).Tcc() > 0;
            for (int i = 0; i < tcc; i++) {
                Do(a.ChildGo(i), b, ref s, isTree2);
            }
            if (isTree)
                s += ")";
        } else {
            s += "\"\"";
        }
    }
    private void Start() {
    }
    public void Button() { }
}
// [ExecuteInEditMode]
// [RequireComponent(typeof(Rigidbody))]
// [RequireComponent(typeof(BoxCollider), typeof(SphereCollider))]
// public class Test : MB
// {
//     [HideInInspector]
//     public bool hideInInspector;
//     [Range(0, 100)]
//     public float range0_100;
//     [Multiline(4)]
//     public string multiline4;
//     [TextArea]
//     public string textArea;
//     [ColorUsage(false, false)]
//     public Color colorUsageFalseFalse;
//     [ColorUsage(false, true)]
//     public Color colorUsageFalseTrue;
//     [ColorUsage(true, false)]
//     public Color colorUsageTrueFalse;
//     [ColorUsage(true, true)]
//     public Color colorUsageTrueTrue;
//     public int spaceStart10;
//     [Space(10)]
//     public int spaceEnd10;
//     [Header("Header")]
//     public int header;
//     [Tooltip("Tooltip")]
//     public int toolTip;
//     [SerializeField]
//     private int serializeField;
//     public SystemSerializable systemSerializable;
//     [System.Serializable]
//     public class SystemSerializable { public int id = 0; }
//     [ContextMenu("ContextMenu")]
//     public void ContextMenu()
//     {
//         tf.localPosition = V3.O;
//         tf.localRotation = Q.id;
//         tf.localScale = V3.I;
//     }
//     public void Button()
//     {
//         // print(A.RepIdx(idx, n));
//         // tf.Translate()
//         // tf.LookAt(V3.O)
//         //     string[] arr = new string[] { "", "l", "r", "b7.5", "b7.5l", "b7.5r" };
//         //     string prvPath = "Main\\Resources\\Rect\\";
//         //     for (int i = 5; i <= 100; i += i == 5 ? 5 : 10) {
//         //         for (int j = 0; j < arr.Length; j++) {
//         //             if ((i == 5 && j == 0) || (i == 10 && j <= 2) || i > 10) {
//         //                 Vector4 border = Vector4.zero;
//         //                 switch (j) {
//         //                     case 0:
//         //                         border = A.SprBorder (i, i, i, i);
//         //                         break;
//         //                     case 1:
//         //                         border = A.SprBorder (i, 0, i, i);
//         //                         break;
//         //                     case 2:
//         //                         border = A.SprBorder (0, i, i, i);
//         //                         break;
//         //                     case 3:
//         //                         border = A.SprBorder (i, i, i, i);
//         //                         break;
//         //                     case 4:
//         //                         border = A.SprBorder (i, 0, i, i);
//         //                         break;
//         //                     case 5:
//         //                         border = A.SprBorder (0, i, i, i);
//         //                         break;
//         //                 }
//         //                 A.SetSprBorder (prvPath + "r" + i + arr[j] + ".png.meta", border);
//         //             }
//         //         }
//         //     }
//     }
// }