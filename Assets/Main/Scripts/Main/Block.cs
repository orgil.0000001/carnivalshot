using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Block : Mb {
    public bool isFall = false;
    float t = 0;
    private void Update() {
        if (IsPlaying && !isFall) {
            if (Tp.y < P.position.y) {
                Fall();
            } else if (V3.Ang(U, V3.u) > 50) {
                t += Dt;
                if (t > 1)
                    Fall();
            } else
                t = 0;
        }
    }
    void Fall() {
        isFall = true;
        P = Ls.I.tf;
        int n = Ls.I.blocks.FindAll(x => x.isFall).Count;
        Cc.I.HudLevelBar(Gc.Level, n.F() / Ls.I.blocks.Count);
        if (n == Ls.I.blocks.Count)
            Gc.I.LevelCompleted();
    }
}
