﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum TargetTp { Red, Blue, Yellow, White, Black }
public enum BlockTp { Alcohol_01, Alcohol_02, Alcohol_03, BeerCup_01, Can_01, Can_02, Cup_01, Drink_01, Drink_02, Drink_03, Drink_04, GlassCup_01, GlassCup_02, Mug_01, ToiletRoll_02 }

[System.Serializable]
public class TargetData {
    public TargetTp tp;
    public Color col;
}

[System.Serializable]
public class Level {
    public Level() {
    }
};

public class Ls : Singleton<Ls> { // Level Spawner
    public Target targetPf;
    public Block blockPf;
    public Table tablePf;
    public List<TargetData> targetDatas;
    public List<Level> levels;
    public bool useLvl = false;
    public GameObject cylPf;
    public Power powerPf;
    public int lvl = 1, ballCnt = 1;
    int lvlCnt = 15, rndLvlMin = 7, rndLvlMax = 15;
    int Lvl => GetLvl(Gc.Level, lvlCnt, rndLvlMin, rndLvlMax);
    int LvlIdx => Lvl - 1;
    float targetW = 0.2f, targetL = 0.005f;
    List<TargetTp> rw = List(TargetTp.Red, TargetTp.White, TargetTp.Red),
        rkwkwk = List(TargetTp.Red, TargetTp.Black, TargetTp.White, TargetTp.Black, TargetTp.White, TargetTp.Black),
        yrbkw = List(TargetTp.Yellow, TargetTp.Red, TargetTp.Blue, TargetTp.Black, TargetTp.White);
    [HideInInspector]
    public List<Block> blocks = new List<Block>();
    Vector3 tableSz = V3.V(3, 0.2f, 1);
    float tableSpd = 1;
    Dictionary<BlockTp, Vector3> blockDic = new Dictionary<BlockTp, Vector3> { };
    public void Init() {
        for (int i = 0; i < blockPf.Tcc; i++) {
            GameObject cGo = blockPf.ChildGo(i);
            blockDic.Add(cGo.name.Enum<BlockTp>(), cGo.Mr().bounds.size);
        }
        if (useLvl)
            Gc.Level = lvl;
        LoadLevel();
        LeaderBoardData.SetDatas();
    }
    public void LoadLevel() {
        int n = Lvl;
        Vector3 pos = V3.V(0, 2, 22), pos2 = pos + V3.Y(tableSz.y / 2);
        if (n == 1) {
            ballCnt = 3;
            CrtBlockPyramid(pos, BlockTp.Alcohol_01, 2);
        } else if (n == 2) {
            ballCnt = 4;
            CrtBlockPyramid(pos - V3.X(3), BlockTp.Alcohol_01, 2);
            CrtBlockPyramid(pos + V3.X(3), BlockTp.Alcohol_02, 2);
        } else if (n == 3) {
            ballCnt = 3;
            CrtBlockPyramid(pos, BlockTp.BeerCup_01, 3);
        } else if (n == 4) {
            ballCnt = 2;
            CrtBlockRect(pos, BlockTp.Alcohol_03, 2, 2);
        } else if (n == 5) {
            ballCnt = 3;
            CrtBlockPyramid(pos, BlockTp.Can_01, 4);
        } else if (n == 6) {
            ballCnt = 4;
            CrtBlockRect(pos, BlockTp.Can_01, 5, 2);
            CrtBlockPyramid(pos + V3.Y(blockDic[BlockTp.Can_01].y * 2), BlockTp.Can_02, 3);
        } else if (n == 7) {
            ballCnt = 3;
            CrtBlockRect(pos, BlockTp.Cup_01, 2, 3);
            CrtBlockPyramid(pos + V3.Y(blockDic[BlockTp.Cup_01].y * 3), BlockTp.Cup_01, 3);
        } else if (n == 8) {
            ballCnt = 8;
            CrtBlockRect(pos, BlockTp.Drink_01, 10, 1);
            CrtBlockRect(pos + V3.Xy(-2, blockDic[BlockTp.Drink_01].y), BlockTp.Drink_01, 2, 2);
            CrtBlock(pos + V3.Xy(-2, blockDic[BlockTp.Drink_01].y * 3), BlockTp.Drink_01);
            CrtBlockRect(pos + V3.Xy(2, blockDic[BlockTp.Drink_01].y), BlockTp.Drink_01, 2, 2);
            CrtBlock(pos + V3.Xy(2, blockDic[BlockTp.Drink_01].y * 3), BlockTp.Drink_01);
            CrtPower(pos + V3.Yz(3.5f, 5), pos, V3.u, 90);
        } else if (n == 9) {
            ballCnt = 3;
            Transform parTf = CrtTable(pos + V3.Xy(-3, 1), pos + V3.Xy(3, 1), 0.5f, tableSpd, tableSz).tf;
            CrtBlockRect(pos2 + V3.Y(1), BlockTp.Drink_02, 3, 1, parTf);
        } else if (n == 10) {
            ballCnt = 5;
            Transform parTf = CrtTable(pos + V3.Xy(-3, 1), pos + V3.Xy(3, 1), 0, tableSpd, tableSz).tf;
            CrtBlockRect(pos2 + V3.Xy(-3, 1), BlockTp.Drink_03, 3, 1, parTf);
            Transform par2Tf = CrtTable(pos + V3.Xy(3, 5), pos + V3.Xy(-3, 5), 0, tableSpd, tableSz).tf;
            CrtBlockRect(pos2 + V3.Xy(3, 5), BlockTp.Drink_03, 3, 1, par2Tf);
        } else if (n == 11) {
            ballCnt = 7;
            Transform parTf = CrtTable(pos + V3.Xy(-3, 1), pos + V3.Xy(3, 1), 0, tableSpd, tableSz.X(4)).tf;
            CrtBlockRect(pos2 + V3.Xy(-3, 1), BlockTp.Drink_04, 4, 1, parTf);
            Transform par2Tf = CrtTable(pos + V3.Xy(3, 5), pos + V3.Xy(-3, 5), 0, tableSpd, tableSz).tf;
            CrtBlockRect(pos2 + V3.Xy(3, 5), BlockTp.Drink_04, 3, 1, par2Tf);
            Transform par3Tf = CrtTable(pos + V3.Xy(-3, 9), pos + V3.Xy(3, 9), 0, tableSpd, tableSz.X(2)).tf;
            CrtBlockRect(pos2 + V3.Xy(-3, 9), BlockTp.Drink_04, 2, 1, par3Tf);
        } else if (n == 12) {
            ballCnt = 8;
            CrtBlockPyramid(pos, BlockTp.GlassCup_01, 4);
            Transform parTf = CrtTable(pos + V3.Xy(-5, 1), pos + V3.Xy(-5, 5), 0, tableSpd, tableSz.X(2)).tf;
            CrtBlockRect(pos2 + V3.Xy(-5, 1), BlockTp.GlassCup_01, 2, 1, parTf);
            Transform par2Tf = CrtTable(pos + V3.Xy(5, 5), pos + V3.Xy(5, 1), 0, tableSpd, tableSz.X(2)).tf;
            CrtBlockRect(pos2 + V3.Xy(5, 5), BlockTp.GlassCup_01, 2, 1, par2Tf);
            CrtPower(pos + V3.Yz(3.5f, 3), pos, V3.u, -90);
        } else if (n == 13) {
            ballCnt = 6;
            Transform parTf = CrtTable(pos + V3.Xy(-3, 0.5f), pos + V3.Xy(3, 0.5f), 0.5f, tableSpd, tableSz.X(7)).tf;
            CrtBlockRect(pos2 + V3.Y(0.5f), BlockTp.GlassCup_02, 5, 1, parTf);
            Transform par2Tf = CrtTable(pos + V3.Y(4.5f), tableSz).tf;
            CrtBlockPyramid(pos2 + V3.Y(4.5f), BlockTp.GlassCup_02, 3, par2Tf);
            CrtPower(pos + V3.Yz(6, 3), pos, V3.u, 90);
        } else if (n == 14) {
            ballCnt = 7;
            Transform parTf = CrtTable(pos + V3.Xy(-3, 1), pos + V3.Xy(-3, 7), 0, tableSpd, tableSz.X(3.5f)).tf;
            CrtBlockRect(pos2 + V3.Xy(-3, 1), BlockTp.Mug_01, 2, 1, parTf);
            Transform par2Tf = CrtTable(pos + V3.Xy(3, 7), pos + V3.Xy(3, 1), 0, tableSpd, tableSz.X(3.5f)).tf;
            CrtBlockRect(pos2 + V3.Xy(3, 7), BlockTp.Mug_01, 2, 1, par2Tf);
            Transform par3Tf = CrtTable(pos + V3.Xy(-3, 7), pos + V3.Xy(3, 7), 0, tableSpd, tableSz.X(3.5f)).tf;
            CrtBlockRect(pos2 + V3.Xy(-3, 7), BlockTp.Mug_01, 2, 1, par3Tf);
            Transform par4Tf = CrtTable(pos + V3.Xy(3, 1), pos + V3.Xy(-3, 1), 0, tableSpd, tableSz.X(3.5f)).tf;
            CrtBlockRect(pos2 + V3.Xy(3, 1), BlockTp.Mug_01, 2, 1, par4Tf);
            CrtPower(pos + V3.Yz(3, 6), pos, V3.u, -90);
        } else if (n == 15) {
            ballCnt = 14;
            Transform parTf = CrtTable(pos + V3.Xy(-3, 1), pos + V3.Xy(3, 1), 0.5f, tableSpd, tableSz.X(9)).tf;
            CrtBlockRect(pos2 + V3.Y(1), BlockTp.ToiletRoll_02, 5, 1, parTf);
            Transform par2Tf = CrtTable(pos + V3.Xy(3, 3), pos + V3.Xy(-3, 3), 0.5f, tableSpd, tableSz.X(7)).tf;
            CrtBlockRect(pos2 + V3.Y(3), BlockTp.ToiletRoll_02, 4, 1, par2Tf);
            Transform par3Tf = CrtTable(pos + V3.Xy(-3, 5), pos + V3.Xy(3, 5), 0.5f, tableSpd, tableSz.X(5)).tf;
            CrtBlockRect(pos2 + V3.Y(5), BlockTp.ToiletRoll_02, 3, 1, par3Tf);
            Transform par4Tf = CrtTable(pos + V3.Xy(3, 7), pos + V3.Xy(-3, 7), 0.5f, tableSpd, tableSz.X(3)).tf;
            CrtBlockRect(pos2 + V3.Y(7), BlockTp.ToiletRoll_02, 2, 1, par4Tf);
            Transform par5Tf = CrtTable(pos + V3.Xy(-3, 9), pos + V3.Xy(3, 9), 0.5f, tableSpd, tableSz.X(1)).tf;
            CrtBlockRect(pos2 + V3.Y(9), BlockTp.ToiletRoll_02, 1, 1, par5Tf);
            CrtPower(pos + V3.Yz(2, 5.5f), pos, V3.u, 90);
        }
        Cc.I.Ball();
    }
    void CrtPower(Vector3 pos, Vector3 pnt, Vector3 axis, float ang) {
        Ins(powerPf, pos, Q.O, tf).Init(pnt, axis, ang);
    }
    void CrtBlockPyramid(Vector3 pos, BlockTp tp, int n, Transform parTf = null) {
        Vector3 sz = blockDic[tp];
        for (int i = n; i > 0; i--)
            for (int j = 0; j < i; j++)
                CrtBlock(pos + V3.Xy((i.StaD() + j) * sz.x, (n - i) * sz.y), tp, parTf);
    }
    void CrtBlockRect(Vector3 pos, BlockTp tp, int w, int h, Transform parTf = null) {
        Vector3 sz = blockDic[tp];
        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++)
                CrtBlock(pos + V3.Xy((w.StaD() + j) * sz.x, i * sz.y), tp, parTf);
    }
    void CrtTarget(Vector3 pos, List<TargetTp> tps, float sz) {
        Target target = Ins(targetPf, pos, Q.O, tf);
        target.Crt(tps, sz, targetW, targetL);
    }
    void CrtBlock(Vector3 pos, BlockTp tp, Transform parTf = null) {
        Block block = Ins(blockPf, pos, Q.O, parTf.Null() ? tf : parTf);
        block.ChildNameShow(tp.tS());
        blocks.Add(block);
    }
    Table CrtTable(Vector3 pos, Vector3 sz) {
        Table table = Ins(tablePf, pos, Q.O, tf);
        table.Child(0).Tls(sz);
        return table;
    }
    Table CrtTable(Vector3 a, Vector3 b, float t, float spd, Vector3 sz) {
        Table table = Ins(tablePf, V3.Lerp(a, b, t), Q.O, tf);
        table.Child(0).Tls(sz);
        table.Init(a, b, t, spd);
        return table;
    }
    int GetLvl(int lvl, int lvlCnt, int rnd1, int rnd2) {
        string lvlData = Data.LevelData.S();
        if (lvlData.IsNe()) {
            for (int i = 1; i <= lvlCnt; i++)
                lvlData += i + ",";
            Data.LevelData.Set(lvlData);
        }
        int[] arr = lvlData.SubLast(0, 1).IntArr();
        if (lvl <= arr.Length) {
            return arr[lvl - 1];
        } else {
            int prvLvl = arr.Last();
            for (int i = arr.Length + 1; i <= lvl; i++) {
                prvLvl = prvLvl == rnd1 ? Rnd.RngIn(rnd1 + 1, rnd2) : prvLvl == rnd2 ? Rnd.RngIn(rnd1, rnd2 - 1) : Rnd.P((prvLvl - rnd1).F() / (rnd2 - rnd1)) ? Rnd.RngIn(rnd1, prvLvl - 1) : Rnd.RngIn(prvLvl + 1, rnd2);
                lvlData += prvLvl + ",";
            }
            Data.LevelData.Set(lvlData);
            return prvLvl;
        }
    }
}
// ░ ▒ ▓ █ ▄ ▀ ■
// ┌ ┬ ┐ ─ ╔ ╦ ╗ ═
// ├ ┼ ┤ │ ╠ ╬ ╣ ║
// └ ┴ ┘   ╚ ╩ ╝
// ⁰¹²³⁴⁵⁶⁷⁸⁹ ⁻⁺⁼⁽⁾ ⁱⁿ superscript
// ₀₁₂₃₄₅₆₇₈₉ ₋₊₌₍₎⨧ ᵣᵤₐᵢⱼₓₑᵥₒₔ ᵪᵧᵦᵨᵩ subscript
// _ ¯ ~ ≡ ‗ ¦ ¨ ¬ · |
// ñ Ñ @ ¿ ? ¡ ! : / \ frequently-used
// á é í ó ú Á É Í Ó Ú vowels acute accent
// ä ë ï ö ü Ä Ë Ï Ö Ü vowels with diaresis
// ½ ¼ ¾ ¹ ³ ² ƒ ± × ÷ mathematical symbols
// $ £ ¥ ¢ ¤ ® © ª º ° commercial / trade symbols
// " ' ( ) [ ] { } « » quotes and parenthesis