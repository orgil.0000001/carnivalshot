﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum PlushTp { Bear_01, Elephant_01, Lion_01, Lioness_01, Rabbit_01, Dino_01, Dino_02, Monster_01, Pig_01, Unicorn_01 }
public enum MatTp { _01_A, _01_B, _01_C, _02_A, _02_B, _02_C, _03_A, _03_B, _03_C, _04_A, _04_B, _04_C }

[System.Serializable]
public class EnvData {
    public List<PlushTp> toys;
    public List<MatTp> mats;
    public Color wallC, wall1C, wall2C, wall3C, floorC, roofC, tableC, line1C, line2C;
    public EnvData(List<PlushTp> toys, List<MatTp> mats, Color wallC, Color wall1C, Color wall2C, Color wall3C, Color floorC, Color roofC, Color tableC, Color lineC1, Color lineC2) {
        this.toys = toys;
        this.mats = mats;
        this.wallC = wallC;
        this.wall1C = wall1C;
        this.wall2C = wall2C;
        this.wall3C = wall3C;
        this.floorC = floorC;
        this.roofC = roofC;
        this.tableC = tableC;
        this.line1C = lineC1;
        this.line2C = lineC2;
    }
}

public class Es : Singleton<Es> { // Environment Spawner
    public GameObject toyPf;
    public Material wallMat, wall1Mat, wall2Mat, wall3Mat, floorMat, roofMat, tableMat, line1Mat, line2Mat;
    public List<EnvData> datas = new List<EnvData>() {
        //new EnvData(
        //    List(PlushTp.Bear_01, PlushTp.Dino_01, PlushTp.Lion_01, PlushTp.Dino_02, PlushTp.Lioness_01, PlushTp.Elephant_01),
        //    List(MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A),
        //    "FFC469".Hex(), "FFE547".Hex(), "FF4242".Hex(), "FFFFFF".Hex(), "BE7C43".Hex(), "FFFAE1".Hex(), "FFF6A5".Hex(), "FF4747".Hex(), "FFFFFF".Hex()),
        //new EnvData(
        //    List(PlushTp.Rabbit_01, PlushTp.Unicorn_01, PlushTp.Pig_01, PlushTp.Elephant_01, PlushTp.Dino_01, PlushTp.Monster_01),
        //    List(MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A),
        //    "69DCFF".Hex(), "47BAFF".Hex(), "4254FF".Hex(), "FFFFFF".Hex(), "437DBE".Hex(), "E1EFFF".Hex(), "BADAFF".Hex(), "47BEFF".Hex(), "FFFFFF".Hex()),
        //new EnvData(
        //    List(PlushTp.Unicorn_01, PlushTp.Monster_01, PlushTp.Rabbit_01, PlushTp.Dino_01, PlushTp.Elephant_01, PlushTp.Dino_02),
        //    List(MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A),
        //    "7EE576".Hex(), "3BF561".Hex(), "63DD00".Hex(), "FFFFFF".Hex(), "62AD47".Hex(), "C8F8CD".Hex(), "A5FFA5".Hex(), "5DE055".Hex(), "FFFFFF".Hex()),
        //new EnvData(
        //    List(PlushTp.Dino_02, PlushTp.Dino_01, PlushTp.Unicorn_01, PlushTp.Monster_01, PlushTp.Rabbit_01, PlushTp.Elephant_01),
        //    List(MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A),
        //    "BEBEBE".Hex(), "BEBEBE".Hex(), "6A6A6A".Hex(), "FFFFFF".Hex(), "939393".Hex(), "DBDBDB".Hex(), "D4D4D4".Hex(), "4F4F4F".Hex(), "FFFFFF".Hex()),
        //new EnvData(
        //    List(PlushTp.Lioness_01, PlushTp.Pig_01, PlushTp.Rabbit_01, PlushTp.Bear_01, PlushTp.Monster_01, PlushTp.Unicorn_01),
        //    List(MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A, MatTp._01_A),
        //    "FFC6F6".Hex(), "FFC4FE".Hex(), "EC70DC".Hex(), "FFFFFF".Hex(), "FF81E9".Hex(), "FFE1FC".Hex(), "FFCEFC".Hex(), "FF81E9".Hex(), "FFFFFF".Hex()),
    };
    public void Init() {
        CrtEnv(datas[(Gc.Level - 1) % datas.Count]);
    }
    public void CrtEnv(EnvData data) {
        for (int i = 0; i < 6; i++) {
            float y = 2 + i * 4, rx = i == 5 ? 30 : 10;
            CrtLine(V3.V(-11, y, 5), V3.V(-11, y, 30), 14, V3.Xy(rx, 90), data.toys[i], data.mats[i]);
            if (i >= 4)
                CrtLine(V3.V(-9, y, 32), V3.V(9, y, 32), 14, V3.Xy(i < 4 ? 30 : rx, 180), data.toys[i], data.mats[i]);
            else
                CrtLine(V3.V(-9, 23, 11 + i * 5), V3.V(9, 23, 11 + i * 5), 14, V3.Xy(30, 180), data.toys[i], data.mats[i]);
            CrtLine(V3.V(11, y, 30), V3.V(11, y, 5), 14, V3.Xy(rx, 270), data.toys[i], data.mats[i]);
        }
        wallMat.color = data.wallC;
        wall1Mat.color = data.wall1C;
        wall2Mat.color = data.wall2C;
        wall3Mat.color = data.wall3C;
        floorMat.color = data.floorC;
        roofMat.color = data.roofC;
        tableMat.color = data.tableC;
        line1Mat.color = data.line1C;
        line2Mat.color = data.line2C;
    }
    public void CrtLine(Vector3 a, Vector3 b, int n, Vector3 rot, PlushTp tp, MatTp matTp) {
        Transform parTf = Crt.NewTf(Tf.PR((a + b) / 2, rot), tf);
        float rndP = 0.3f, rndR = 10;
        Material mat = A.Load<Material>("Toy/PolygonKids_Material" + matTp);
        for (int i = 0; i < n; i++) {
            GameObject toyGo = Ins(toyPf, V3.Lerp(a, b, i.F() / (n - 1)) + V3.V(Rnd.Rng(-rndP, rndP), Rnd.Rng(-rndP, rndP), Rnd.Rng(-rndP, rndP)),
                Q.E(rot) * Q.Xy(Rnd.Rng(-rndR, rndR), Rnd.Rng(-rndR, rndR)), parTf);
            GameObject cGo = toyGo.ChildNameGo(tp.tS());
            cGo.RenMat(mat);
            cGo.Show();
        }
    }
    public void ColumnsCol(Material columnMat, Material bgMat, Color columnCol, Color bgUCol, Color bgDCol, Vector2 fogY) {
        columnMat.color = columnCol;
        columnMat.SetFloat("_HeightFogStart", fogY.x);
        columnMat.SetFloat("_HeightFogEnd", fogY.y);
        columnMat.SetColor("_HeightFogColor", bgDCol);
        bgMat.color = bgUCol;
        bgMat.SetColor("_HeightFogColor", bgDCol);
    }
    public void CrtColumns(GameObject columnPf, Transform columnsTf, List<float> xs, float dx, Vector2 y, Vector2 sz, Vector2 spc, Vector2 fogY, float len) {
        for (int i = 0; i < xs.Count; i++)
            for (float z = 0; z < len;) {
                float colSz = sz.Rnd(), colX = xs[i].RndD(dx), colY = y.Rnd();
                GameObject columnGo = Ins(columnPf, V3.V(colX, (colY + fogY.x) / 2f, z + colSz), Q.O, columnsTf);
                columnGo.Tls(V3.V(colSz, colY - fogY.x, colSz));
                z += colSz + spc.Rnd();
            }
    }
    public void CrtBlds(string path, float roadW, float l) {
        List<BoxCollider> bldPfs = Resources.LoadAll<BoxCollider>(path).List();
        for (int i = 0; i < 2; i++) {
            bool isR = i == 1;
            List<GameObject> lis = new List<GameObject>();
            for (int j = 0; (lis.IsEmpty() ? true : lis.Last().Tp().z < l) && j < 100; j++) {
                BoxCollider bc = bldPfs.Rnd();
                float lstZ = lis.IsEmpty() ? 0 : lis.Last().Tp().z + lis.Last().BcSz().x / 2 + lis.Last().BcCen().x * isR.Sign();
                Vector3 pos = V3.V((roadW / 2 + bc.size.z / 2 + bc.center.z) * isR.Sign(), bc.size.y / 2 - bc.center.y, lstZ + bc.size.x / 2 - bc.center.x * isR.Sign());
                GameObject bldGo = Ins(bc.gameObject, pos, Q.Y(isR ? -90 : 90), tf);
                lis.Add(bldGo);
            }
        }
    }
}