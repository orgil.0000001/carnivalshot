using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Target : Mb {
    public List<TargetTp> tps;
    public float sz = 2, w = 0.1f, l = 0.005f;
    public bool isStaCrt = false;
    private void Start() {
        if (isStaCrt)
            Crt(tps, sz, w, l);
    }
    public void Crt(List<TargetTp> tps, float sz, float w, float l) {
        for (int i = 0; i < tps.Count; i++) {
            GameObject cylGo = Ins(Ls.I.cylPf, Tp, Tr * Q.X(-90), tf);
            cylGo.Tls(V3.V((i + 1) * sz / tps.Count).Y(w / 2));
            cylGo.Tlp(V3.Z((i - tps.Count + 1) * l));
            cylGo.RenMatCol(Ls.I.targetDatas.Find(x => x.tp == tps[i]).col);
            if (i == tps.Count - 1)
                cylGo.Mc(true).convex = true;
        }
        rb.G();
    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.Tag(Tag.Ball)) {
        }
    }
}
