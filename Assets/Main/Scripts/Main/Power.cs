using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Power : Mb {
    public Vector3 pnt, axis;
    public float ang;
    public void Init(Vector3 pnt, Vector3 axis, float ang) {
        this.pnt = pnt;
        this.axis = axis;
        this.ang = ang;
    }
    void Update() {
        tf.RotateAround(pnt, axis, Dt * ang);
    }
}
