using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class PowerBar : Mb {
    public float Pwr => 1 - M.Abs(M.PingPong01(power) - 0.5f) * 2;
    [Range(0f, 1f)]
    public float power = 0;
    public int n = 20;
    public float ang = 120, rI = 0.5f, rO = 1, w = 0.1f, l = 0.1f, angL = 5, pntY = 1;
    public GameObject barGo, bgGo, pntGo;
    void Start() {
        Upd(barGo, n, ang, rI, rO, w);
        Upd(bgGo, n, ang + angL, rI - l, rO + l, w);
        pntGo.TlpY(pntY);
    }
    void Update() {
        power += Dt * Player.I.powerBarSpd;
        pntGo.Par().Tle(V3.Z(M.Remap(M.PingPong01(power), 0, 1, ang / 2, -ang / 2)));
    }
    void Upd(GameObject go, int n, float ang, float rI, float rO, float w) {
        Mesh mesh = null;
        Msh.Init(ref mesh, go);
        Msh msh = new Msh();
        for (int i = 0; i <= 4; i++) {
            for (int j = 0; j <= n; j++) {
                float a = 90 + ang / 2 - ang * j / n, r = i == 1 || i == 2 ? rO : rI, z = i == 2 || i == 3 ? w / 2 : -w / 2;
                msh.vs.Add(V3.V(M.Cos(a) * r, M.Sin(a) * r, z));
                msh.uv.Add(V2.V(j.F() / n, 0));
            }
        }
        for (int i = 0; i < 4; i++)
            msh.ts.TsLoop(n, i * (n + 1), 1, (i + 1) * (n + 1), 1, true, 1, 10000);
        msh.ts.FcI(0, n + 1, n * 2 + 2, n * 3 + 3);
        msh.ts.Fc(n, n * 2 + 1, n * 3 + 2, n * 4 + 3);
        msh.Upd(ref mesh);
    }
}
