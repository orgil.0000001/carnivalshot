namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, Diamond, Bot, Coin, Ball, Wall, Block, Power }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Water = 4, UI = 5, Plush = 6, Wall = 7; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Water = 16, UI = 32, Plush = 64, Wall = 128; }
}