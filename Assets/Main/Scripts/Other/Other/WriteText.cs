using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using UnityEngine.UI;

public class WriteText : Mb {
    public Text txt;
    public float writeTime = 0.1f, staWaitTime = 0.5f, endWaitTime = 2f;
    private void Awake() {
        if (txt.Null())
            txt = go.Txt();
    }
    public void Show(string text) {
        StaCor(ShowCor(text, writeTime, staWaitTime, endWaitTime));
    }
    IEnumerator ShowCor(string text, float writeTime, float staWaitTime, float endWaitTime) {
        txt.text = "_";
        WaitForSeconds wfs = Wf.S(writeTime);
        yield return Wf.S(staWaitTime);
        for (int i = 0; i < text.Length; i++) {
            yield return wfs;
            txt.text = text.Sub(0, i + 1) + "_";
        }
        yield return Wf.S(endWaitTime);
        txt.text = "";
    }
}
