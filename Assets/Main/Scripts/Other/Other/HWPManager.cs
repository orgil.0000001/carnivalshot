﻿using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class HWPManager : Mb {
    [Tooltip("Hud list manager, you can add a new hud directly here.")]
    public List<HWPInfo> Huds = new List<HWPInfo>();
    [Tooltip("You can use MainCamera or the root of your player")]
    public Transform LocalPlayer = null;
    public float offScreenArrowDis = 0, offScreenNameDis = 100;
    public float clampBorder = 12;
    public bool useGizmos = true;
    [Header("Global Settings")]
    [Range(1, 50)] public float IconSize = 50;
    [Range(1, 50)] public float OffScreenIconSize = 25;
    [Header("GUI Scaler")]
    [Tooltip("The resolution the UI layout is designed for. If the screen resolution is larger, the GUI will be scaled up, and if it's smaller, the GUI will be scaled down. This is done in accordance with the Screen Match Mode.")]
    public Vector2 m_ReferenceResolution = new Vector2(800f, 600f);
    [Range(0f, 1f), Tooltip("Determines if the scaling is using the width or height as reference, or a mix in between."), SerializeField]
    public float m_MatchWidthOrHeight;
    [Tooltip("Select Reference Resolution automatically in run time.")]
    public bool AutoScale = true;
    [Header("Style")]
    public GUIStyle TextStyle;
    private static HWPManager _I;
    public static HWPManager I { get { if (!_I) _I = GameObject.FindObjectOfType<HWPManager>(); return _I; } }
    void OnDestroy() {
        _I = null;
    }
    void Update() { if (AutoScale) { m_ReferenceResolution.x = Screen.width; m_ReferenceResolution.y = Screen.height; } }
    void OnGUI() {
        if (!HWPUtility.mCam || !LocalPlayer || Gc.I.followType == FollowType.None)
            return;
        //pass test :)
        for (int i = 0; i < Huds.Count; i++) {
            if (!Huds[i].Hide) {
                if (Huds[i].HideOnCloseDistance > 0 && GetHudDistance(i) < Huds[i].HideOnCloseDistance) { continue; }
                if (Huds[i].HideOnLargeDistance > 0 && GetHudDistance(i) > Huds[i].HideOnLargeDistance) { continue; }
                OnScreen(i);
                OffScreen(i);
            }
        }
    }
    void OnScreen(int i) {
        //if transform destroy, then remove from list
        if (!Huds[i].m_Target) {
            Huds.Remove(Huds[i]);
            return;
        }
        //Check target if OnScreen
        if (Huds[i].m_Target &&
            HWPUtility.IsOnScreen(HWPUtility.ScreenPos(Huds[i].m_Target), Huds[i].m_Target) &&
            IsPlaying && // Тоглох
            !Huds[i].character ? true : Huds[i].character.data.isLive && // амьд
            (Gc.I.followType == FollowType.Hud || Gc.I.followType == FollowType.HudPointer || Gc.I.followType == FollowType.HudPointerOff)) // HUD
        {
            //Calculate Position of target
            Vector3 RelativePosition = Huds[i].m_Target.position + Huds[i].Offset;
            if ((V3.Dot(this.LocalPlayer.forward, RelativePosition - this.LocalPlayer.position) > 0f)) {
                //Calculate the 2D position of the position where the icon should be drawn
                Vector3 point = HWPUtility.mCam.WorldToViewportPoint(RelativePosition);
                //The viewportPoint coordinates are between 0 and 1, so we have to convert them into screen space here
                Vector2 drawPosition = new Vector2(point.x * Screen.width, Screen.height * (1 - point.y));
                if (!Huds[i].arrow.ShowArrow) {
                    //Clamp the position to the edge of the screen in case the icon would be drawn outside the screen
                    drawPosition.x = M.C(drawPosition.x, clampBorder, Screen.width - clampBorder);
                    drawPosition.y = M.C(drawPosition.y, clampBorder, Screen.height - clampBorder);
                }
                //Calculate distance from player to way point               Cache distance
                float dis = V3.Dis(LocalPlayer.position, RelativePosition), dis2 = dis;
                //Max Hud Increment 
                if (dis > Huds[i].m_MaxSize) // if more than "50" no increase more
                    dis = 50;
                float n = IconSize;
                //Calculate depend of type 
                if (Huds[i].m_TypeHud == HWPType.Dec)
                    n = (((50 + dis) / (25)) * 0.9f) + 0.1f;
                else if (Huds[i].m_TypeHud == HWPType.Inc)
                    n = (((50 - dis) / (25)) * 0.9f) + 0.1f;
                //Calculate Size of Hud
                float sizeX = Huds[i].m_Icon.width * n;
                if (sizeX >= Huds[i].m_MaxSize)
                    sizeX = Huds[i].m_MaxSize;
                float sizeY = Huds[i].m_Icon.height * n;
                if (sizeY >= Huds[i].m_MaxSize)
                    sizeY = Huds[i].m_MaxSize;
                float TextUperIcon = sizeY / 2 + 5;
                //palpating effect
                if (Huds[i].isPalpitin)
                    Palpating(Huds[i]);
                //Draw Huds
                GUI.color = Huds[i].color;
                GUI.DrawTexture(new Rect(drawPosition.x - (sizeX / 2), drawPosition.y - (sizeY / 2), sizeX, sizeY), Huds[i].m_Icon);
                if (!Huds[i].ShowDistance) {
                    if (!string.IsNullOrEmpty(Huds[i].text)) {
                        Vector2 size = TextStyle.CalcSize(new GUIContent(Huds[i].text));
                        GUI.Label(new Rect(drawPosition.x - (size.x / 2) + 10, (drawPosition.y - (size.y / 2)) - TextUperIcon, size.x, size.y), Huds[i].text, TextStyle);
                    }
                } else {
                    if (!string.IsNullOrEmpty(Huds[i].text)) {
                        string txt = Huds[i].text + "\n<color=whitte>[" + string.Format("{0:N0}m", dis2) + "]</color>";
                        Vector2 sz = TextStyle.CalcSize(new GUIContent(txt));
                        GUI.Label(new Rect(drawPosition.x - (sz.x / 2) + 10, (drawPosition.y - (sz.y / 2)) - TextUperIcon, sz.x, sz.y), txt, TextStyle);
                    } else {
                        string txt = "<color=whitte>[" + string.Format("{0:N0}m", dis2) + "]</color>";
                        Vector2 sz = TextStyle.CalcSize(new GUIContent(txt));
                        GUI.Label(new Rect(drawPosition.x - (sz.x / 2) + 10, ((drawPosition.y - (sz.y / 2)) - TextUperIcon), sz.x, sz.y), txt, TextStyle);
                    }
                }
            }
        }
    }
    void OffScreen(int i) {
        //if transform destroy, then remove from list
        if (!Huds[i].m_Target) {
            Huds.Remove(Huds[i]);
            return;
        }
        if (Huds[i].arrow.ArrowIcon && Huds[i].arrow.ShowArrow &&
            IsPlaying && // Тоглох
            !Huds[i].character ? true : Huds[i].character.data.isLive && // амьд
            Gc.Kills + Gc.I.showFollowLive >= Bs.I.characters.Count && // showFollowLive хүнтэй
            (Gc.I.followType != FollowType.Follow && Gc.I.followType != FollowType.Hud)) // заагчтай
        {
            //Check target if OnScreen
            if (!HWPUtility.IsOnScreen(HWPUtility.ScreenPos(Huds[i].m_Target), Huds[i].m_Target)) {
                //Get the relative position of arrow
                Vector3 arrowPos = Huds[i].m_Target.position + Huds[i].arrow.ArrowOffset;
                Vector3 arrowPnt = HWPUtility.mCam.WorldToScreenPoint(arrowPos).Div(HWPUtility.mCam.pixelWidth, HWPUtility.mCam.pixelHeight, 1);
                Vector3 fwd = Huds[i].m_Target.position - HWPUtility.mCam.Tp();
                Vector3 dir = HWPUtility.mCam.TfInvDir(fwd).normalized / 5;
                arrowPnt.x = 0.5f + dir.x * 20f / HWPUtility.mCam.aspect;
                arrowPnt.y = 0.5f + dir.y * 20f;
                if (arrowPnt.z < 0) {
                    arrowPnt *= -1f;
                    arrowPnt *= -1f;
                }
                //Arrow
                GUI.color = Huds[i].color;
                float xPos = HWPUtility.mCam.pixelWidth * arrowPnt.x, yPos = HWPUtility.mCam.pixelHeight * (1f - arrowPnt.y);
                //palpating effect
                if (Huds[i].isPalpitin)
                    Palpating(Huds[i]);
                //Calculate area to rotate guis
                float mRot = HWPUtility.GetRot(HWPUtility.mCam.pixelWidth / (2), HWPUtility.mCam.pixelHeight / (2), xPos, yPos);
                //Get pivot from area
                Vector2 pivot = HWPUtility.GetPivot(xPos, yPos, Huds[i].arrow.size);
                //Arrow
                Vector2 rotVec = new Vector2(M.Cos(mRot), M.Sin(mRot));
                Vector2 pos1 = pivot - rotVec * offScreenArrowDis;
                Matrix4x4 matrix = GUI.matrix;
                GUIUtility.RotateAroundPivot(mRot - 90, pos1);
                GUI.DrawTexture(new Rect(pos1.x - HWPUtility.HalfSz(Huds[i].arrow.size), pos1.y - HWPUtility.HalfSz(Huds[i].arrow.size), Huds[i].arrow.size, Huds[i].arrow.size), Huds[i].arrow.ArrowIcon);
                GUI.matrix = matrix;
                GUI.DrawTexture(HWPUtility.SclRect(new Rect(M.C(pivot.x, 20, (Screen.width - OffScreenIconSize) - 20), M.C(pivot.y, 20, (Screen.height - OffScreenIconSize) - 20), OffScreenIconSize, OffScreenIconSize)), Huds[i].m_Icon);
                Vector2 pos = pivot;
                //Icons and Text
                if (Gc.I.followType != FollowType.FollowPointerOff && Gc.I.followType != FollowType.HudPointerOff)
                    if (!Huds[i].ShowDistance) {
                        if (!string.IsNullOrEmpty(Huds[i].text)) {
                            Vector2 size = TextStyle.CalcSize(new GUIContent(Huds[i].text));
                            // pos.x = M.Clamp (pos.x, (size.x + OffScreenIconSize) + 30, ((Screen.width - OffScreenIconSize) - 10) - size.x);
                            // pos.y = M.Clamp (pos.y, (size.y + OffScreenIconSize) + 35, ((Screen.height - size.y) - OffScreenIconSize) - 20);
                            pos = pivot - rotVec * offScreenNameDis;
                            GUIUtility.RotateAroundPivot(mRot - 90, pos);
                            GUI.Label(HWPUtility.SclRect(new Rect(
                                pos.x - (size.x / 2),
                                pos.y - (size.y / 2),
                                size.x, size.y)), Huds[i].text, TextStyle);
                            GUI.matrix = matrix;
                        }
                    } else {
                        float Distance = V3.Dis(LocalPlayer.position, Huds[i].m_Target.position);
                        if (!string.IsNullOrEmpty(Huds[i].text)) {
                            string text = Huds[i].text + "\n <color=whitte>[" + string.Format("{0:N0}m", Distance) + "]</color>";
                            Vector2 size = TextStyle.CalcSize(new GUIContent(text));
                            pos.x = M.C(pos.x, (size.x + OffScreenIconSize) + 30, ((Screen.width - OffScreenIconSize) - 10) - size.x);
                            pos.y = M.C(pos.y, (size.y + OffScreenIconSize) + 35, ((Screen.height - size.y) - OffScreenIconSize) - 20);
                            GUI.Label(HWPUtility.SclRect(new Rect(pos.x - (size.x / 2), (pos.y - (size.y / 2)), size.x, size.y)), text, TextStyle);
                        } else {
                            string text = "<color=whitte>[" + string.Format("{0:N0}m", Distance) + "]</color>";
                            Vector2 size = TextStyle.CalcSize(new GUIContent(text));
                            pos.x = M.C(pos.x, (size.x + OffScreenIconSize) + 30, ((Screen.width - OffScreenIconSize) - 10) - size.x);
                            pos.y = M.C(pos.y, (size.y + OffScreenIconSize) + 35, ((Screen.height - size.y) - OffScreenIconSize) - 20);
                            GUI.Label(HWPUtility.SclRect(new Rect(pos.x - (size.x / 2), (pos.y - (size.y / 2)), size.x, size.y)), text, TextStyle);
                        }
                    }
                // GUI.DrawTexture(bl_HudUtility.ScalerRect(new Rect(mPivot.x + marge.x,(mPivot.y + ((!Huds[i].ShowDistance) ? 10 : 20)) + marge.y, 25, 25)), Huds[i].m_Icon);
            }
            GUI.color = C.I;
        }
    }
    public void CreateHud(HWPInfo info) {
        Huds.Add(info);
    }
    public void RemoveHud(int i) {
        Huds.RemoveAt(i);
    }
    public void RemoveHud(HWPInfo hud) {
        if (Huds.Contains(hud))
            Huds.Remove(hud);
        else
            Dbg.Log("Huds list don't contain this hud!");
    }
    public void HideStateHud(int i, bool hide = false) {
        if (Huds[i].NotNull())
            Huds[i].Hide = hide;
    }
    public void HideStateHud(HWPInfo hud, bool hide = false) {
        if (Huds.Contains(hud))
            for (int i = 0; i < Huds.Count; i++)
                if (Huds[i] == hud)
                    Huds[i].Hide = hide;
    }
    private float GetHudDistance(int i) {
        if (Huds[i].Null() || !Huds[i].m_Target) { // if transform destroy, then remove from list
            Huds.Remove(Huds[i]);
            return 0;
        }
        return V3.Dis(LocalPlayer.position, Huds[i].m_Target.position + Huds[i].Offset); // Calculate Position of target
    }
    private void Palpating(HWPInfo hud) {
        if (hud.color.a <= 0)
            hud.tip = false;
        else if (hud.color.a >= 1)
            hud.tip = true;
        //Create a loop
        if (hud.tip == false)
            hud.color.a += Dt * 0.5f;
        else
            hud.color.a -= Dt * 0.5f;
    }
    void OnDrawGizmosSelected() {
        if (!useGizmos)
            return;
        for (int i = 0; i < Huds.Count; i++) {
            if (Huds[i].m_Target) {
                Gizmos.color = new Color(0, 0.35f, 0.9f, 0.9f);
                Gizmos.DrawWireSphere(Huds[i].m_Target.position, 3);
                Gizmos.color = new Color(0, 0.35f, 0.9f, 0.3f);
                Gizmos.DrawSphere(Huds[i].m_Target.position, 3);
                if (i < Huds.Count - 1)
                    Gizmos.DrawLine(Huds[i].m_Target.position, Huds[i + 1].m_Target.position);
                else
                    Gizmos.DrawLine(Huds[i].m_Target.position, Huds[0].m_Target.position);
            }
        }
    }
}