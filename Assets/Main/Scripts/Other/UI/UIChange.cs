﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

[ExecuteInEditMode]
public class UIChange : Mb {
    [Range(0f, 1f)]
    public float alpha = 1;
    public Color color, color2;
    public Font font;
    public TMP_FontAsset tmpFont;
    List<TextMeshPro> tmpLis = new List<TextMeshPro>();
    List<TextMesh> tmLis = new List<TextMesh>();
    List<Text> txtLis = new List<Text>();
    List<Image> imgLis = new List<Image>();
    List<float> tmpALis = new List<float>();
    List<float> tmALis = new List<float>();
    List<float> txtALis = new List<float>();
    List<float> imgALis = new List<float>();
    private void Update() {
        alpha = M.C01(alpha);
        for (int i = 0; i < tmpLis.Count; i++)
            tmpLis[i].color = tmpLis[i].color.A(tmpALis[i] * alpha);
        for (int i = 0; i < tmLis.Count; i++)
            tmLis[i].color = tmLis[i].color.A(tmALis[i] * alpha);
        for (int i = 0; i < txtLis.Count; i++)
            txtLis[i].color = txtLis[i].color.A(txtALis[i] * alpha);
        for (int i = 0; i < imgLis.Count; i++)
            imgLis[i].color = imgLis[i].color.A(imgALis[i] * alpha);
    }
    public void Alpha() {
        tmpLis = go.Gca<TextMeshPro>();
        tmpLis.ForEach(x => tmpALis.Add(x.color.a));
        tmLis = go.Gca<TextMesh>();
        tmLis.ForEach(x => tmALis.Add(x.color.a));
        txtLis = go.Gca<Text>();
        txtLis.ForEach(x => txtALis.Add(x.color.a));
        imgLis = go.Gca<Image>();
        imgLis.ForEach(x => imgALis.Add(x.color.a));
    }
    public void Color() {
        A.FOsOT<TextMeshPro>().ForEach(x => x.color = x.color == color ? color2 : x.color);
        A.FOsOT<TextMesh>().ForEach(x => x.color = x.color == color ? color2 : x.color);
        A.FOsOT<Text>().ForEach(x => x.color = x.color == color ? color2 : x.color);
        A.FOsOT<Image>().ForEach(x => x.color = x.color == color ? color2 : x.color);
        A.SaveOpenScenes();
    }
    public void Font() {
        A.FOsOT<TextMeshPro>().ForEach(x => x.font = tmpFont);
        A.FOsOT<TextMesh>().ForEach(x => x.font = font);
        A.FOsOT<Text>().ForEach(x => x.font = font);
        A.SaveOpenScenes();
    }
}